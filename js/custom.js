$('.answer').slideUp(0);

$('.qa').click(function() {

    var clickedAnswer = $(this).find('.answer');

    $('.answer').not(clickedAnswer).slideUp();

    clickedAnswer.slideToggle();

    var clickedArrow = $(this).find('.question span');

    $('.question span').not(clickedArrow).removeClass('open');

    clickedArrow.toggleClass('open');

});

// JS to load when document is ready
$(document).ready(function(){

    // Opening new page using filter
    var hashtag = window.location.hash;

    if (hashtag.startsWith("#filter-")){
        var value = hashtag.replace("#filter-" , "");
        $(".filter").not('.'+value).hide('3000');
        $('.filter').filter('.'+value).show('3000');
    }

    // Filter
    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            window.location.hash = "";
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            window.location.hash = "#filter-" + value;
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            
        }
    });
    
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});

function myFunction() {
    alert("Thank you. The Bears have received your request and will process it accordingly.");
  }